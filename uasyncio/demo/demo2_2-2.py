# -*- coding: utf-8 -*-
# @company : Quectel
# @Time    : 2021/9/10 9:22
# @Author  : Jaxsen Xu

from usr import uasyncio as asyncio
async def bar(x):
    count = 0
    while True:
        count += 1
        print('Instance: {} count: {}'.format(x, count))
        await asyncio.sleep(1)  # Pause 1s

async def main():
    for x in range(3):
        asyncio.create_task(bar(x))
    print('Tasks are running')
    await asyncio.sleep(10)

asyncio.run(main())
