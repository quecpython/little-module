# uasyncio

## code



里面是代码,直接将文件夹里面内容导入到固件里面运行,就可以通过以下方式使用

```python
import usr.uasyncio as asyncio
```



## demo

demo里面有一些示例, 可以用于测试



## docs

里面是uasyncio的文档



## 其他功能

请查看micropython的相关asyncio文档https://github.com/peterhinch/micropython-async/blob/master/v3/docs/TUTORIAL.md